# Museon calendar

Een bolt extensie om een wekelijks overzicht te maken voor de museon site

## usage

Deze extensie geeft de volgende twig tags: `{{ rendercalendar }}` en `{% setcalendar() %}`

De opties voor `setcalendar` zijn:

```twig
    {% set calendaritems = setcalendar(
            {
                "startdate": start, 
                "enddate": end, 
                'activitygroups': activitygroups
            }
        ) 
    %}
```

De activitygroups kunnen ook leeg zijn.
        
En vergelijkbaar voor `rendercalendar`

```twig 
    {{ rendercalendar(
            {
                "startdate": start, 
                "enddate": end, 
                'activitygroups': activitygroups
            }
        ) 
    }}
```

De rendercalendar zal het twig template `@Calendar/calendar.twig` gebruiken voor het renderen, en die kun je dus op die manier eventueel overriden.