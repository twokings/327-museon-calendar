<?php

namespace Bolt\Extension\TwoKings\MuseonCalendar;

use Bolt\Extension\SimpleExtension;
use Silex\Application;
use Silex\ControllerCollection;

/**
 * MuseonCalendar extension class.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class MuseonCalendarExtension extends SimpleExtension
{

    /**
     * {@inheritdoc}
     */
    protected function registerTwigFunctions()
    {
        return [
            'setcalendar' => 'calendarFunction',
            'rendercalendar' => ['calendarRender', ['is_safe' => ['html']]]
        ];
    }

    /**
     * Get the calendar items in a week (depending on start and end date)
     *
     * @param array $options ['startdate': \DateTime $start, 'enddate': \DateTime $end]
     * @return array
     */
    private function getCalendar($options)
    {
        $app = $this->getContainer();

        // prepare options
        $options['now'] = date_create();

        if ($options['startdate']) {
            $startdate = $options['startdate'];
        } else {
            $startdate = $options['now'];
        }
        if ($options['enddate']) {
            $enddate = $options['enddate'];
        } else {
            $enddate = date_add($options['now'], '+6 days');
        }

        // returns an array of arrays (i.e. a raw data set)
        $qb = $app['storage']->createQueryBuilder();
        $qb->select('distinct(bfstart.content_id)')
            ->from('bolt_field_value', 'bfstart')
            ->innerJoin('bfstart', 'bolt_field_value', 'bfend', 'bfstart.content_id = bfend.content_id AND bfstart.grouping = bfend.grouping')
            ->where('bfstart.fieldname = :typestart')
            ->andWhere('bfend.fieldname = :typeend')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $qb->expr()->lte('bfstart.value_date', ':enddate'),
                        $qb->expr()->gte('bfend.value_date', ':startdate')
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->lte('UNIX_TIMESTAMP(bfstart.value_datetime)',
                            'UNIX_TIMESTAMP(:enddatetime)'),
                        $qb->expr()->gte('UNIX_TIMESTAMP(bfend.value_datetime)',
                            'UNIX_TIMESTAMP(:startdatetime)'),
                        $qb->expr()->isNull('bfend.value_date')
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->lte('UNIX_TIMESTAMP(bfstart.value_datetime)',
                            'UNIX_TIMESTAMP(:enddatetime)'),
                        $qb->expr()->gte('UNIX_TIMESTAMP(bfstart.value_datetime)',
                            'UNIX_TIMESTAMP(:startdatetime)'),
                        $qb->expr()->isNull('bfend.value_datetime')
                    )
                )
            )
            ->setParameter('typestart', 'startdate')
            ->setParameter('typeend', 'enddate')
            ->setParameter('startdate', $startdate->format('Y-m-d'))
            ->setParameter('enddate',  $enddate->format('Y-m-d'))
            ->setParameter('startdatetime', $startdate->format('Y-m-d 00:00:00'))
            ->setParameter('enddatetime',  $enddate->format('Y-m-d 23:59:59'))
        ;

        if (is_string($options['activitygroups']) && $options['activitygroups'] !== 'false') {
            $qb->innerJoin('bfstart', 'bolt_taxonomy', 'tax', 'bfstart.content_id = tax.content_id AND tax.contenttype = :contenttype')
                ->andWhere('tax.taxonomytype = :taxtype')
                ->andWhere('tax.slug = :taxslug')
                ->setParameter('contenttype', 'activities')
                ->setParameter('taxtype', 'activitygroups')
                ->setParameter('taxslug', $options['activitygroups']);
        }

        $calendariditems = $qb->execute()->fetchAll();

        $repo = $app['storage']->getRepository('activities');
        // translate the content_id's to a flattened array
        $calendarids = [];
        $calendaritems = [];
        foreach($calendariditems as $item) {
            $id = $item['content_id'];
            $calendarids[] = $id ;
            $calendaritem = $repo->findBy(['id' => $id])[0];
            $calendaritems[] = $calendaritem;
        }

        return $calendaritems;
    }

    /**
     * Return the calendar items
     *
     * @param $options
     * @return array
     */
    public function calendarFunction($options)
    {
        $context['calendar'] = $this->getCalendar($options);
        $context['options'] = $options;
        $context['config'] = $this->getConfig();

        return $context;
    }

    /**
     * Render the calendar items
     *
     * @param $options
     * @return string
     */
    public function calendarRender($options)
    {
        $context['calendar'] = $this->getCalendar($options);
        $context['options'] = $options;
        $context['config'] = $this->getConfig();

        return $this->renderTemplate('@Calendar/calendar.twig', $context);
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigPaths()
    {
        return [
            'templates'   => ['namespace' => 'Calendar'],
        ];
    }
    /**
     * Read the configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return parent::getConfig();
    }

    /**
     * Provide default configuration
     *
     * @return array
     */
    protected function getDefaultConfig()
    {
        return [
            'numberofweeks' => 10
        ];
    }
}